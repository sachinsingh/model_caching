# ModelCaching

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'model_caching', git: 'https://rodrigora@bitbucket.org/rodrigora/model_caching.git'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install model_caching

## Usage

	This gem will add these methods to all models:

	def cached_find id
		Rails.cache.fetch([self.name, :cached_find, id]) { self.find(id) }
	end

	def flush_find
		Rails.cache.delete([self.class.name, :cached_find, id])
	end

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
