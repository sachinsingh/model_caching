require 'model_caching'

ActiveRecord::Base.establish_connection(:adapter => "sqlite3", 
                                       :database => File.dirname(__FILE__) + "/db/model_caching.sqlite3")

# Load schema
load File.dirname(__FILE__) + '/support/schema.rb'

# Create ActiveRecord models
load File.dirname(__FILE__) + '/support/models.rb'

# Create recors to test
load File.dirname(__FILE__) + '/support/data.rb'