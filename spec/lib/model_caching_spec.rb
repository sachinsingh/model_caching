require 'spec_helper'

describe ModelCaching do

	it 'should contains VERSION' do
		ModelCaching::VERSION.should_not be_nil
	end

	it 'has cached_find method' do
		User.should respond_to :cached_find
	end

	it 'has flush_find method' do
		User.new.should respond_to :flush_find
	end

	it 'should set flush_find on after_commit callback' do
		User._commit_callbacks.select {|cb| cb.kind == :after}.collect(&:filter).should include :flush_find
	end

	pending "cached_find should call Rails.cache.fetch"
	pending "flush_find should call Rails.cache.delete"
end